package main



import (

	"flag"
	"log"
	"time"

	"bitbucket.org/cocoon_bitbucket/ptfsupervision/subscribers"
	"bitbucket.org/cocoon_bitbucket/ptfsupervision/publishers"
)


func main() {

	var(

		addr              = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")
		redisSubscribeUrl = flag.String("redis-url", "redis://localhost:6379", "The address to redis subscribe.")
		ptfChannel        = flag.String("subscribe-channel", "ptf/WifiStats_wl0/*", "The channel set to subscribe")
	)
	flag.Parse()

	// wait for a redis server to be up
	time.Sleep( 2 *time.Second)

	log.Printf("Create Redis Pool for %s\n",*redisSubscribeUrl)
	pool := subscribers.GetRedisPool(* redisSubscribeUrl)

	// create subscriber for wifi stats and launch it
	pub := subscribers.RedisSubscriber{pool,*ptfChannel, publishers.PrometheusPublish}
	pub.Spawn()

	// create subscriber for WAnMode ( NMC.WanMode )
	c := publishers.NewWAnModeCollector()
	c.Init()
	p2 := subscribers.RedisSubscriber{pool,"ptf/PPP_DHCP/*", c.Publish}
	p2.Spawn()



	// launch prometheus server
	publishers.RunPrometheusServer( addr )


	return
}
