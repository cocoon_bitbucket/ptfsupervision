package publishers

import (
	"strconv"
	"github.com/prometheus/client_golang/prometheus"
	"log"
)



type ICollector interface {

	Init()
	Publish(topic string, response  Response, device string) (err error)
}



type Collector struct {

	gauges map[string] * prometheus.GaugeVec
	counters map[string] * prometheus.CounterVec

}



func NewWAnModeCollector () ICollector {

	c := Collector{}
	c.gauges =  make(map[string] * prometheus.GaugeVec )
	c.counters =  make(map[string] * prometheus.CounterVec )
	return c
}

// create metrics for NMC.WanMode and register to prometheus
func ( c Collector  ) Init() {

	labels :=  []string{"device",}

	c.gauges["livebox_dsl_ppp"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "livebox_dsl_ppp",
		Help: "livebox wan mode is ppp",
	},labels)

	c.gauges["livebox_dsl_dhcp"] = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "livebox_dsl_dhcp",
		Help: "livebox wan mode is dhcp",
	},labels)


	// register gauges to prometheus
	for name, collector := range c.gauges{
		log.Printf("register prometheus gauge collector: %s\n",name)
		prometheus.MustRegister(collector)
	}

}

// publlish metrics
func ( c Collector) Publish( topic string, response  Response, device string) (err error) {

	timestamp :=  strconv.Itoa(int(response.Timestamp))

	data :=  response.Response.(map[string]interface{})
	data["_timestamp"] = timestamp
	data["_request"] = response.Request

	// for each element of response ( normaly only one: NMC.WanMode )
	for field,value := range data {

		switch field {
		case "NMC.WanMode":
			// the value is either DSL_PPP or DSL_DHC
			switch value {
			case "DSL_PPP":
				c.gauges["livebox_dsl_ppp"].WithLabelValues(device,).Set(1)
				c.gauges["livebox_dsl_dhcp"].WithLabelValues(device,).Set(0)
			case "DSL_DHCP":
				c.gauges["livebox_dsl_dhcp"].WithLabelValues(device,).Set(1)
				c.gauges["livebox_dsl_ppp"].WithLabelValues(device,).Set(0)
			}
		}
	}

	return err
}



