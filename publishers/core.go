package publishers

import (

	//"errors"

	"github.com/prometheus/client_golang/prometheus"
	"log"
)


var (

	// global counter for subscription received  subscribe_message_received ( device= , channel= )

	SubscribedMessageCounter prometheus.CounterVec


)



//
// gauges
//

type GaugeDescription [][3]string


type GaugeCollector struct {
	Field string					// field name eg Device.WiFi.SSID.wl0.Stats.PacketsSent
	Collector * prometheus.GaugeVec
}
type GaugeCollectors []GaugeCollector


func InitGauges ( gauges * GaugeCollectors,  description GaugeDescription, labels ...string){

	// update the gauges with collectors from gauges description
	for _,description := range description{
		field := description[0]
		name := description[1]
		help := description[2]
		// create gauge
		collector := prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: name,
			Help: help,
		},labels)
		// add it to all gauges
		* gauges = append( * gauges, GaugeCollector{field, collector})

		// register it
		log.Printf("register prometheus gauge collector: %s for field: %s",name,field)
		prometheus.MustRegister(collector)
	}
}


//
// counters
//


type CounterDescription [][3]string

type CounterCollector struct {
	Field string
	Collector * prometheus.CounterVec
}
type CounterCollectors []CounterCollector


func InitCounters ( counters * CounterCollectors,  description CounterDescription, labels ...string){

	// update the gauges with collectors from gauges description
	for _,description := range description{
		field := description[0]
		name := description[1]
		help := description[2]
		// create counter collector
		collector := prometheus.NewCounterVec(prometheus.CounterOpts{
			Name: name,
			Help: help,
		},labels)
		// add it to all gauges
		* counters = append( * counters, CounterCollector{field, collector})


		// register it
		log.Printf("register prometheus counter collector: %s for field: %s",name,field)
		prometheus.MustRegister(collector)
	}
}


//
//  global metric subscribed_message_received
//
func IncSubscribedMessageReceived(channel,device string){

	SubscribedMessageCounter.WithLabelValues(channel,device).Inc()

}




func InitSubcriptionStats(){

	// init metrics for all message received by subscription
	 SubscribedMessageCounter = * prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "subscribed_message_received",
		Help: "Number of redis pubsub message received",
	},[]string{"channel","device"})

}




//func init() {
//
//	InitSubcriptionStats()
//}