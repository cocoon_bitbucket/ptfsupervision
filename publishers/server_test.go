package publishers_test



import (

	"testing"
	"flag"

	"bitbucket.org/cocoon_bitbucket/ptfsupervision/subscribers"
	"bitbucket.org/cocoon_bitbucket/ptfsupervision/publishers"

)



func TestPrometheusServer(t *testing.T) {

	var(

	addr              = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

	redisSubscribeUrl = flag.String("redis-url", "redis://localhost:6379", "The address to redis subscribe.")
	ptfChannel      = flag.String("subscribe channel", "ptf/WifiStats_wl0/*", "The channel set to subscribe")
	)

	pool := subscribers.GetRedisPool(* redisSubscribeUrl)

	// create subscriber and launch it
	pub := subscribers.RedisSubscriber{pool,*ptfChannel, publishers.PrometheusPublish}
	pub.Spawn()


	// launch prometheus server
	publishers.RunPrometheusServer( addr )


	return
}