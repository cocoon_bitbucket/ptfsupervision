package publishers

import (
	"strconv"
)



/*

	prometheus probe for livebox wifi stats


	listen to pubsub chanel ptf/WifiStats_wl0/*

	and export as a prometheus target http server localhost:8080/metrics

	labels: livebox


	ptf/WifiStats_wl0/192.168.1.1 : {
	"_ip":"192.168.1.1",
	"_err":null,
	"_timestamp":1519837338,
	"_request":"Device.WiFi.SSID.wl0.Stats",
	"response":{
		"Device.WiFi.SSID.wl0.Stats.BroadcastPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.BroadcastPacketsSent":"0",
		"Device.WiFi.SSID.wl0.Stats.BytesReceived":"5268200",
		"Device.WiFi.SSID.wl0.Stats.BytesSent":"105194015",
		"Device.WiFi.SSID.wl0.Stats.DiscardPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.DiscardPacketsSent":"121",
		"Device.WiFi.SSID.wl0.Stats.ErrorsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.ErrorsSent":"121",
		"Device.WiFi.SSID.wl0.Stats.FailedRetransCount":"0",
		"Device.WiFi.SSID.wl0.Stats.MulticastPacketsReceived":"1194",
		"Device.WiFi.SSID.wl0.Stats.MulticastPacketsSent":"1276109",
		"Device.WiFi.SSID.wl0.Stats.PacketsReceived":"32540",
		"Device.WiFi.SSID.wl0.Stats.PacketsSent":"1223293",
		"Device.WiFi.SSID.wl0.Stats.RetransCount":"0",
		"Device.WiFi.SSID.wl0.Stats.UnicastPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.UnicastPacketsSent":"0",
		"Device.WiFi.SSID.wl0.Stats.UnknownProtoPacketsReceived":"0"
		}
	}

 */


var (

	// domain is either an ip or a name
	labels = []string{"device",}

	// describe gauges : Field / Name / Help
	gauges_description = GaugeDescription {

		{"Device.WiFi.SSID.wl0.Stats.PacketsSent",		"wifi_packet_sent", "Number of packets send via wifi"},
		{"Device.WiFi.SSID.wl0.Stats.PacketsReceived",	"wifi_packet_received", "wifi_packet_received"},
		{"Device.WiFi.SSID.wl0.Stats.ErrorsSent",		"wifi_errors_sent", "Number of wifi errors sent"},
		{"Device.WiFi.SSID.wl0.Stats.ErrorsReceived",	"wifi_errors_received", "Number of wifi error received"},
		{"Device.WiFi.SSID.wl0.Stats.DiscardPacketsSent","wifi_discard_packets_sent", "Number of discarded wifi packets sent"},
		{"Device.WiFi.SSID.wl0.Stats.DiscardPacketsReceived","wifi_discard_packets_received","Number of discarded wifi packets sent"},

	}

	gauges  GaugeCollectors


)

func init() {
	// Register the summary and the histogram with Prometheus's default registry.

	InitGauges( &gauges, gauges_description,labels...)

}


// publish to prometheus
func PrometheusPublish( topic string, response  Response, device string) (err error) {

	timestamp :=  strconv.Itoa(int(response.Timestamp))

	data :=  response.Response.(map[string]interface{})
	data["_timestamp"] = timestamp
	data["_request"] = response.Request


	for _,gauge := range gauges {

		if value,ok := data[gauge.Field] ; ok {
			i,err := strconv.Atoi(value.(string))
			if err == nil {
				gauge.Collector.WithLabelValues(device,).Set(float64(i))
			}
		}
	}

	return err
}



