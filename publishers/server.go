package publishers

import (

	"net/http"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
)



func RunPrometheusServer( addr * string){

	// addr string eg "localhost:8080
	if *addr == "" {
		* addr = ":8080"
	}

	// Expose the registered metrics via HTTP.
	http.Handle("/metrics", promhttp.Handler())
	log.Printf("Launch prometheus server listening at %s/metrics\n",*addr)
	log.Fatal(http.ListenAndServe(*addr, nil))

}
