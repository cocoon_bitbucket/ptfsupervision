package subscribers

import (

	"strings"
	"errors"
	"fmt"
	"log"
	"encoding/json"
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfsupervision/publishers"
)
//
//  publisher
//




type RedisSubscriber struct {


	Pool redis.Pool

	Channel string   // ptf/DeviceInfo/*

	Publish publishers.Publisher    // publish function
}


func ( r * RedisSubscriber) Spawn(){
	go r.Run()
}


func ( r * RedisSubscriber) Run(){

	log.Printf("Launch Subscriber for channel: %s\n",r.Channel)


	cnx := r.Pool.Get()
	defer cnx.Close()

	db := r.Pool.Get()
	defer db.Close()

	sub := redis.PubSubConn{Conn: cnx}
	sub.PSubscribe(r.Channel)


	// receive loop
	for {
		switch v := sub.Receive().(type) {
		case redis.PMessage:

			//fmt.Printf("receive a PMessage on channel %s : %s\n",v.Channel,v.Data)
			err := r.HandleMessage(db,v.Channel,v.Data)
			if err != nil {
				fmt.Printf("Handle message error: %s",err.Error())
			}

		case redis.Subscription:
			fmt.Printf("subscription message: %s: %s %d\n", v.Channel, v.Kind, v.Count)

		case error:
			fmt.Println("error pub/sub, delivery has stopped")
			return
		}
	}

}


func (r * RedisSubscriber ) SplitChannel(channel string) (prefix,topic,ip string, err error){
	// split the channel ptf/DeviceInfo/192.168.1.1 into prefix/topic/ip
	if strings.Contains(channel,"/"){
		parts := strings.Split(channel,"/")
		if len(parts) == 3 {
			return parts[0],parts[1],parts[2],err
		}
	}
	err = errors.New("Bad channel: " + channel)
	return "","","",err
}

func (r * RedisSubscriber ) HandleMessage(cnx redis.Conn, channel string, message []byte) (err error){


	_,topic,ip , err := r.SplitChannel(channel)
	if err != nil {
		return err
		//fmt.Print(err.Error())
	}

	response := publishers.Response{}
	err = json.Unmarshal(message,&response)
	if err != nil {
		//fmt.Printf("error: %s\n",err.Error())
		return err
	}

	if response.Response == nil {
		// an empty response received
		err = errors.New("empty response received for topic: " + topic)
		return err
	}

	err = r.Publish(topic,response,ip)

	return err
}


