package subscribers



import(


	"github.com/garyburd/redigo/redis"
	"time"
)


func GetRedisPool( redisUrl string) redis.Pool {
	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}



//import (
//
//
//	"strconv"
//	"fmt"
//	"encoding/json"
//
//)
//
//
//type Publisher  func( string, Response, string) error
//
//
//
//func SamplePublisher( topic string, response Response, domain string) (err error) {
//	//
//	// domain is either ip or name
//	//
//	timestamp :=  strconv.Itoa(int(response.Timestamp))
//
//	data :=  response.Response.(map[string]interface{})
//	data["_timestamp"] = timestamp
//	data["_request"] = response.Request
//
//	for k,v := range data{
//		fmt.Printf("key: %s, value: %s\n",k,v)
//	}
//	return err
//}
//
//
//
////
//// Response
////
//
//type Response struct {
//	// response to a request
//
//	Ip string    			`json:"_ip"`
//	Err error				`json:"_err"`
//	Timestamp int32			`json:"_timestamp"`
//	Request string			`json:"_request"`
//	Response interface{} 	`json:"response"`
//}
//func (r * Response )Jsonify() ([]uint8,error) {
//	b, err := json.Marshal(r)
//	return b,err
//}
//
//
