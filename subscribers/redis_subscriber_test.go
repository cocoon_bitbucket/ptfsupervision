package subscribers_test

import (
	"testing"
	"time"
	"github.com/garyburd/redigo/redis"

	"bitbucket.org/cocoon_bitbucket/ptfsupervision/subscribers"
	"bitbucket.org/cocoon_bitbucket/ptfsupervision/publishers"
)


func get_pool() redis.Pool {

	var redisUrl= "redis://localhost:6379/0"

	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}






func TestRedisSubscriber(t *testing.T) {


	pool := get_pool()


	reporter := subscribers.RedisSubscriber{ pool, "ptf/DeviceInfo/*", publishers.SamplePublisher}


	reporter.Run()


	return
}