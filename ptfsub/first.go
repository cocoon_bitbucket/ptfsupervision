
// Copyright 2015 The Prometheus Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// A simple example exposing fictional RPC latencies with different types of
// random distributions (uniform, normal, and exponential) as Prometheus
// metrics.


/*

	prometheus probe for livebox wifi stats


	listen to pubsub chanel ptf/WifiStats_wl0/*

	and export as a prometheus target http server localhost:8080/metrics

	labels: livebox


	ptf/WifiStats_wl0/192.168.1.1 : {
	"_ip":"192.168.1.1",
	"_err":null,
	"_timestamp":1519837338,
	"_request":"Device.WiFi.SSID.wl0.Stats",
	"response":{
		"Device.WiFi.SSID.wl0.Stats.BroadcastPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.BroadcastPacketsSent":"0",
		"Device.WiFi.SSID.wl0.Stats.BytesReceived":"5268200",
		"Device.WiFi.SSID.wl0.Stats.BytesSent":"105194015",
		"Device.WiFi.SSID.wl0.Stats.DiscardPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.DiscardPacketsSent":"121",
		"Device.WiFi.SSID.wl0.Stats.ErrorsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.ErrorsSent":"121",
		"Device.WiFi.SSID.wl0.Stats.FailedRetransCount":"0",
		"Device.WiFi.SSID.wl0.Stats.MulticastPacketsReceived":"1194",
		"Device.WiFi.SSID.wl0.Stats.MulticastPacketsSent":"1276109",
		"Device.WiFi.SSID.wl0.Stats.PacketsReceived":"32540",
		"Device.WiFi.SSID.wl0.Stats.PacketsSent":"1223293",
		"Device.WiFi.SSID.wl0.Stats.RetransCount":"0",
		"Device.WiFi.SSID.wl0.Stats.UnicastPacketsReceived":"0",
		"Device.WiFi.SSID.wl0.Stats.UnicastPacketsSent":"0",
		"Device.WiFi.SSID.wl0.Stats.UnknownProtoPacketsReceived":"0"
		}
	}

 */



package main

import (

	"flag"
	"log"
	//"math"
	//"math/rand"
	"net/http"
	"time"
	//"errors"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	messages "bitbucket.org/cocoon_bitbucket/ptfsupervision"
	"bitbucket.org/cocoon_bitbucket/ptfsupervision/subscribers"

	"github.com/garyburd/redigo/redis"
)


//type Response subscribers.Response


var (
	addr              = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

	redisSubscribeUrl = flag.String("redis url", "redis://localhost:6379", "The address to redis subscribe.")
	ptfChannel      = flag.String("subscribe channel", "ptf/WifiStats_wl0/*", "The channel set to subscribe")


)

var (

	// domain is either an ip or a name
	labels = []string{"domain",}

	// Create guages for wifi wl0 stats

	wifiPacketSent = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "wifi_packet_sent",
		Help: "Number of packets send via wifi",
	})

	wifiPacketReceived = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "wifi_packet_received",
		Help: "Number of packets received via wifi",
	})


	wifiErrorsSent = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "wifi_errors_sent",
		Help: "Number of wifi errors sent",
		}, labels)

	wifiErrorsReceived = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "wifi_errors_received",
		Help: "Number of wifi error received",
		}, labels)


)

func init() {
	// Register the summary and the histogram with Prometheus's default registry.
	prometheus.MustRegister(wifiPacketSent)
	prometheus.MustRegister(wifiPacketReceived)

}


func main() {
	flag.Parse()


	pool := get_pool(*redisSubscribeUrl)

	pub := subscribers.RedisSubscriber{pool,*ptfChannel, PrometheusPublish}

	go func() {
		pub.Run()
	}()

	// Expose the registered metrics via HTTP.
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(*addr, nil))
}



// publish to prometheus
func PrometheusPublish( topic string, response  messages.Response, domain string) (err error) {

	timestamp :=  strconv.Itoa(int(response.Timestamp))

	data :=  response.Response.(map[string]interface{})
	data["_timestamp"] = timestamp
	data["_request"] = response.Request


	if value,ok := data["Device.WiFi.SSID.wl0.Stats.PacketsSent"] ; ok {
		i,err := strconv.Atoi(value.(string))
		if err == nil {
			wifiPacketSent.Set(float64(i))
		}
	}

	if value,ok := data["Device.WiFi.SSID.wl0.Stats.PacketsReceived"] ; ok {
		i,err := strconv.Atoi(value.(string))
		if err == nil {
			wifiPacketReceived.Set(float64(i))
		}
	}

	if value,ok := data["Device.WiFi.SSID.wl0.Stats.ErrorsSent"] ; ok {
		i,err := strconv.Atoi(value.(string))
		if err == nil {
			wifiErrorsSent.WithLabelValues("domain",).Set(float64(i))
		}
	}

	if value,ok := data["Device.WiFi.SSID.wl0.Stats.ErrorsReceived"] ; ok {
		i,err := strconv.Atoi(value.(string))
		if err == nil {
			wifiErrorsReceived.WithLabelValues("domain",).Set(float64(i))
		}
	}
	return err
}



func get_pool( redisUrl string) redis.Pool {
	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}
